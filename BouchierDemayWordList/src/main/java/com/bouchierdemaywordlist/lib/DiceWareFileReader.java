/*
 */
package com.bouchierdemaywordlist.lib;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

/**
 *
 * @author demay
 * @author bouchier
 */
public class DiceWareFileReader {

    // <editor-fold defaultstate="collapsed" desc="Attributs">
    private HashMap<Integer, String> hashMap = new HashMap<>();
    private String source;
    private BufferedReader bufferReader;

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Constructeur">
    public DiceWareFileReader(String path) throws FileNotFoundException, IOException {
        this.source = path;
        this.bufferReader = new BufferedReader(new FileReader(source));
        String key;
        int value;
        String line = this.bufferReader.readLine(); // on ouvre du flux du fileReader ici
        try {
            while ((line != null)) {
                value = Integer.parseInt(line.substring(0, 5));
                this.hashMap.put(value, line.substring(6));
                line = this.bufferReader.readLine();
            }
        } catch (IOException e) {
            System.out.println("ERROR : Une erreur a eu lieu pendant la lecture du BufferReader");
        } finally {
            this.bufferReader.close(); // Fermeture obligatoire du flux
        }
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Setters et Getters">
    public HashMap<Integer, String> getHashMap() {
        return hashMap;
    }

    public void setHashMap(HashMap<Integer, String> hashMap) {
        this.hashMap = hashMap;
    }

    // </editor-fold>
}
