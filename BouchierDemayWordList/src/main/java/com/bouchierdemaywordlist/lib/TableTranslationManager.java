/*
 */
package com.bouchierdemaywordlist.lib;

/**
 *
 * @author thibault
 * @author bouchier
 */
public class TableTranslationManager {

    // <editor-fold defaultstate="collapsed" desc="Attributs">
    private DiceWareFileReader fileReader;

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Constructeur">
    public TableTranslationManager(DiceWareFileReader fileReader) {
        this.fileReader = fileReader;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Méthodes">
    //INPUT : un entier
    //OUPUT : un String issu de l'interrogation de la hashmap
    public String translate(int number) {
        String translation = fileReader.getHashMap().get(number);
        System.out.println("translation for " + number + " is " + translation);
        return translation;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters">
    public DiceWareFileReader getFileReader() {
        return fileReader;
    }

    public void setFileReader(DiceWareFileReader fileReader) {
        this.fileReader = fileReader;
    }
    // </editor-fold> 
}
