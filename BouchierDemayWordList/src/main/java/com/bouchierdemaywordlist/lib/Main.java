/*
 */
package com.bouchierdemaywordlist.lib;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author demay
 * @author bouchier
 */
public class Main {

    //On peut passer un ou deux arguments. Les deux sont optionnels.
    //INPUT : ?String levelPassword, ?String pathFile
    public static void main(String[] args) throws IOException {
        
        
        System.out.println("-----------------------------------Début du Projet !--------------------------------------------\n");
        String pathFileDefault = "ressources\\francais_wordlist.txt"; // notre fichier texte par défaut
        String pathFile = pathFileDefault;
        int levelPassWordDefault = 5;
        int choiceLevel = levelPassWordDefault;
        if(args.length > 0) //Mode Automatique Commande Line
        {
            System.out.println("Arguments entrés, changement du mode par default : \n");
            try
            {  
                choiceLevel = Integer.parseInt(args[0]);
                System.out.println("Nouveau default levelPassword = " + choiceLevel + "\n");
            }
            catch(NumberFormatException e)
            {
                System.out.println("Le premier argument n'est pas un chiffre, le level par defaut reste donc " + choiceLevel + "\n");
            }
            if(args.length > 1)
            {
                pathFile = args[1];
                System.out.println("Nouveau default pathFile = " + pathFile + "\n");
            }
        }
        else // Mode Manuel Console
        {
        // <editor-fold defaultstate="collapsed" desc="Interface Console">
        // Deux informations demandés au User : Quel dictionnaire utiliser, et quel niveau du mot de passe il souhaite.
        
            BufferedReader brInputUser = new BufferedReader(new InputStreamReader(System.in));
            System.out.print(String.format("\nBienvenue dans notre générateur de mot de passe DiweWare."
                    + "\nLe dictionnaire de traduction par défaut doit se situer à l'adresse suivante : %s."
                    + "\nSouhaitez vous utiliser un autre dictionnaire ? Format txt, veuillez respecter le formalisme du dictionnaire par défaut."
                    +"\nEntrez 1 pour le mode par défaut, 2 pour utiliser un dictionnaire personnalisé.\n", pathFileDefault));
            int choixDictionnaire = 1; // valeur par défaut
            try {
                choixDictionnaire = Integer.parseInt(brInputUser.readLine());
            } catch (NumberFormatException nfe) {
                System.err.println("ERROR : Ceci n'est pas un entier. Choix par défaut enclenché.\n");
            }
            switch (choixDictionnaire) {
                case 2:
                    System.out.println("Vous avez choisi d'entrer un nouveau dictionnaire, saisissez son emplacement :\n");
                    pathFile = brInputUser.readLine();
                    break;
                default:  // mode par défaut rien ne change
                    break;
            }
            System.out.println(String.format("Quel niveau de mot de passe souhaitez vous ?"
            + "\nEntrez un entier compris entre 1 et 10. Appuyez sur Entrée pour le niveau par défaut = %d.\n", choiceLevel));
             try {
                choiceLevel = Integer.parseInt(brInputUser.readLine());
            } catch (NumberFormatException nfe) {
                System.err.println("Ceci n'est pas un entier. Choix par défault enclenché.\n");
            }
        }

        // </editor-fold>
        try 
        {    
            run(choiceLevel, pathFile);
        }
        catch (NullPointerException e)
        {
            System.out.println(e);
            if(pathFile == null ? pathFileDefault != null : !pathFile.equals(pathFileDefault))
            {
                System.out.println("\nNouvel essai avec le path par default : " + pathFileDefault+" a la place de : "+pathFile);
                run(choiceLevel, pathFileDefault);
            }
        }

        System.out.println("\n------------------------------------Fin du Projet !---------------------------------------------");
    }
    
    public static void run(int choiceLevel, String pathFile)
    {
        DiceWareManager diceWareManager = new DiceWareManager(pathFile); // la classe principale
        String myPassword = "";
        try {
            myPassword = diceWareManager.createPassword(choiceLevel);
        } catch (OutOfMemoryError | StackOverflowError e) {
            System.err.print(e);
        } catch (IllegalArgumentException e) {
            System.err.print(e);
        }finally {
            System.out.println("Password : " + myPassword);
        }
    }
}
