/*
 */
package com.bouchierdemaywordlist.lib;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Random;

/**
 *
 * @author demay
 * @author bouchier
 */
public final class DiceWareManager {

    // <editor-fold defaultstate="collapsed" desc="Attributs">
    private TableTranslationManager tableTranslationManager;

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Constructeurs">
    public DiceWareManager(String pathDWFileReader) {
        try {
            DiceWareFileReader fileReader = new DiceWareFileReader(pathDWFileReader);
            setTableTranslationManager(new TableTranslationManager(fileReader));
        } catch (FileNotFoundException e) {
            System.out.println("ERROR : Fichier non trouvé dans FileReader");
        } catch (IOException e) {
            System.out.println("ERROR : IOException dans FileReader");
        }

    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Méthodes">
    //INPUT : un int level représentant le nombre de mot dans le mot de passe
    //OUPUT : le String password
    public String createPassword(int level) {
        if (level <= 0) {
            throw new IllegalArgumentException("Level cant be setup bellow 1, you entered : " + level + "\n");
        }

        String password = "";
        for (int i = 0; i < level; i++) {
            password += getTableTranslationManager().translate(rollDices());
        }

        return password;
    }

    //INPUT:
    //OUTPUT: Un entier à cinq chiffre compris entre 1 et 6 du type {11111 - 66666}
    public int rollDices() {
        int[] rollResults = new int[5];

        for (int i = 0; i < 5; i++) {
            rollResults[i] = rollDice();
        }

        return concatenateIntArray(rollResults);
    }

    //INPUT: Un tableau d'entier
    //OUTPUT: Un entier issu de la concaténation des entiers du tableau en input
    public int concatenateIntArray(int[] intArray) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < intArray.length; i++) {
            builder.append(intArray[i]);
        }

        return Integer.parseInt(builder.toString());
    }

    //INPUT: 
    //OUPUT : Un entier compris entre 1 et 6
    public int rollDice() {
        Random rand = new Random();

        return rand.nextInt(5) + 1;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Getters and Setters">
    public TableTranslationManager getTableTranslationManager() {
        return tableTranslationManager;
    }

    public void setTableTranslationManager(TableTranslationManager tableTranslationManager) {
        this.tableTranslationManager = tableTranslationManager;
    }

    // </editor-fold>
}
