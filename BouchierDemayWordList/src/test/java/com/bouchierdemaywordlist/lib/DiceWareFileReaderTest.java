/*
 */
package com.bouchierdemaywordlist.lib;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 *
 * @author demay
 * @author bouchier
 */
public class DiceWareFileReaderTest {

    public DiceWareFileReaderTest() {
    }

// @ParameterizedTest(name = "Using value {0}")
    @ValueSource(ints = {1, 2, 3, 4, 5, 6})
    public void aaaaaaaa(int i) {
        //assertTrue(!diceWareManager.createPassword(level).isEmpty());
    }

    @Test
    public void notFoundException() {
        String fakePathString = "monfauxfichier.txt";

        assertThrows(FileNotFoundException.class, () -> {
            DiceWareFileReader diceWareFileReaderTest = new DiceWareFileReader(fakePathString);
        });
    }

    @Test
    public void gettingAllLines() {
        String pathFile = "ressources\\francais_wordlist_short.txt";
        try {
            DiceWareFileReader diceWareFileReader = new DiceWareFileReader(pathFile);
            assertEquals(10, diceWareFileReader.getHashMap().size());
        } catch (FileNotFoundException e) {
            System.out.println(String.format("ERROR : Fichier %s non trouvé", pathFile));
        } catch (IOException e) {
            System.out.println("ERROR : I/O Exception ! ");
        }
    }

    @Test
    public void gettingAllValuesAndKeys() {
        String pathFile = "ressources\\francais_wordlist_short.txt";
        try {
            DiceWareFileReader diceWareFileReader = new DiceWareFileReader(pathFile);
            HashMap<Integer, String> hashmap = diceWareFileReader.getHashMap();
            for (Entry<Integer, String > entry : hashmap.entrySet()) {
                int clef = entry.getKey();
                String valeur = entry.getValue();
            }
            assertEquals(10, hashmap.size()); // retourne 10, le nombre de ligne du fichier short
            assertEquals("jurais", hashmap.get(35652)); // des valeurs dans le fichier
            assertEquals("jury", hashmap.get(35665));
            assertEquals("jures", hashmap.get(35661));
            // TODO : Coder un plantage car pas dans la map assertEquals("jures", hashmap.get(35661));
        } catch (FileNotFoundException e) {
            System.out.println(String.format("ERROR : Fichier %s non trouvé", pathFile));
        } catch (IOException e) {
            System.out.println("ERROR : I/O Exception ! ");
        }
    }
}
