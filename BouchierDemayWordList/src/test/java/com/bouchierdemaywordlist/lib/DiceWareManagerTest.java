/*
 */
package com.bouchierdemaywordlist.lib;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import static org.hamcrest.CoreMatchers.allOf;
import static org.junit.Assert.assertThat;

/**
 *
 * @author demay
 * @author bouchier
 */
public class DiceWareManagerTest {
    
    public DiceWareManagerTest() {
    }
    
    String pathFile = "ressources\\francais_wordlist_short.txt";
    
    
    @ParameterizedTest(name = "Using value {0}")
    @ValueSource(ints = {1,2,3,4,5,6})
    public void testCreatePasswordWorking(int level) 
    {
        DiceWareManager diceWareManager = new DiceWareManager(pathFile);
        assertTrue(!diceWareManager.createPassword(level).isEmpty());
    }
    
    @ParameterizedTest(name = "Using value {0}")
    @ValueSource(ints = {-1,0})
    public void testCreatePasswordIllegalException(int level)
    {
        DiceWareManager diceWareManager = new DiceWareManager(pathFile);
        assertThrows(IllegalArgumentException.class, () -> {
            diceWareManager.createPassword(level);
        });
    }
    
    @Test
    public void testConcatenateIntArray()
    {
        int[] intArray = {1,2,3,4,5,6};
        
        DiceWareManager diceWareManager = new DiceWareManager(pathFile);
        assertEquals(diceWareManager.concatenateIntArray(intArray), 123456);
    }
    
    @Test
    public void testRollDice()
    {
        int low = 0;
        int high = 7;
        
        DiceWareManager diceWareManager = new DiceWareManager(pathFile);
        
        assertTrue("Error, random is too high", high >= diceWareManager.rollDice());
        assertTrue("Error, random is too low",  low  <= diceWareManager.rollDice());
    }
    
    @Test
    public void testRollDices()
    {
        int low = 11111;
        int high = 66666;
        
        DiceWareManager diceWareManager = new DiceWareManager(pathFile);
        
        assertTrue("Error, random is too high", high >= diceWareManager.rollDices());
        assertTrue("Error, random is too low",  low  <= diceWareManager.rollDices());
    }
}
